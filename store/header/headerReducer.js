import * as types from "../actionTypes";

const initialState = {
  headerRoutes: [],
};
const headerReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_HEADER_NAV_OPTIONS: {
      const newState = { ...state, headerRoutes: action.headerRoutes };
      return newState;
    }
    default:
      return state;
  }
};

export default headerReducer;
