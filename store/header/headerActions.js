import axios from "axios";
import { URIS } from "../../constants/urls";
import * as types from "../actionTypes";

function setHeaderNavOptions(headerRoutes) {
  return {
    type: types.SET_HEADER_NAV_OPTIONS,
    headerRoutes,
  };
}

export function fetchHeaderOptions() {
  return (dispatch) => {
    axios
      .get(URIS.HEADER_ROUTER)
      .then((response) => {
        console.debug(response.data.menu.items);
        dispatch(setHeaderNavOptions(response.data.menu.items));
      })
      .catch((error) => {
        console.error(error);
        // TODO Agregar manejo de errores
      });
  };
}
