import { combineReducers } from "redux";
import headerReducer from "./header/headerReducer";
import calculatorReducer from "./pages/calculatorReducer";
import testimonialsReducer from "./pages/testimonialsReducer";

// COMBINED REDUCERS
const reducers = {
  Header: headerReducer,
  TestimonialsPage: testimonialsReducer,
  CalculatorPage: calculatorReducer
};

export default combineReducers(reducers);
