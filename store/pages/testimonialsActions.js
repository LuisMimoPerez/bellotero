import axios from "axios";
import { URIS } from "../../constants/urls";
import * as types from "../actionTypes";

function setTestimonialsPageTitle(title) {
  return {
    type: types.SET_TESTIMONIALS_TITLE,
    title,
  };
}
function setTestimonialsPageTestimonials(testimonials) {
  return {
    type: types.SET_TESTIMONIALS,
    testimonials,
  };
}

export function fetchTestimonialPageInfo() {
  return (dispatch) => {
    // TODO SE PODRIA CHACHEAR EN EL CLIENTE POR UN DETERMINADO TIEMPO
    axios
      .get(URIS.TESTIMONIALS_PAGE)
      .then((response) => {
        console.debug(response.data);
        dispatch(setTestimonialsPageTitle(response.data.slider.title));
        dispatch(setTestimonialsPageTestimonials(response.data.slider.reviews));
      })
      .catch((error) => {
        console.error(error);
        // TODO Agregar manejo de errores
      });
  };
}
