export const calculatorPageTitle = (state) => state.CalculatorPage.title;
export const calculatorPageDescription = (state) =>
  state.CalculatorPage.description;
export const calculatorPageFullTimeEmployees = (state) =>
  state.CalculatorPage.employees;
export const calculatorPageMonthlyIngredientSpending = (state) =>
  state.CalculatorPage.monthlyIngredient;
export const testimonialsPageTitle = (state) => state.TestimonialsPage.title;
export const testimonialsPageTestimonials = (state) =>
  state.TestimonialsPage.testimonials;
