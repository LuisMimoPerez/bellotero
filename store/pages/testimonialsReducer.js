import * as types from "../actionTypes";

const initialState = {
  title: "",
  testimonials: [],
};
const testimonialsReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_TESTIMONIALS: {
      const newState = { ...state, testimonials: action.testimonials };
      return newState;
    }
    case types.SET_TESTIMONIALS_TITLE: {
      const newState = { ...state, title: action.title };
      return newState;
    }
    default:
      return state;
  }
};

export default testimonialsReducer;
