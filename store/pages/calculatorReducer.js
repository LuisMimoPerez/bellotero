import * as types from "../actionTypes";

const initialState = {
  title: "",
  description: "",
  monthlyIngredient: 10,
  employees: 1,
};
const calculatorReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_CALCULATOR_TITLE: {
      const newState = { ...state, title: action.title };
      return newState;
    }
    case types.SET_CALCULATOR_DESCRIPTION: {
      const newState = { ...state, description: action.description };
      return newState;
    }
    case types.SET_CALCULATOR_MONTHLY_INGREDIENT_SPENDING: {
      const newState = {
        ...state,
        monthlyIngredient: action.monthlyIngredient,
      };
      return newState;
    }
    case types.SET_CALCULATOR_FULLTIME_EMPLOYEES: {
      const newState = { ...state, employees: action.employees };
      return newState;
    }
    default:
      return state;
  }
};

export default calculatorReducer;
