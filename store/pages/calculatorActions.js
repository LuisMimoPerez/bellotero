import axios from "axios";
import { URIS } from "../../constants/urls";
import * as types from "../actionTypes";

function setCalculatorPageTitle(title) {
  return {
    type: types.SET_CALCULATOR_TITLE,
    title,
  };
}
function setCalculatorPageDescription(description) {
  return {
    type: types.SET_CALCULATOR_DESCRIPTION,
    description,
  };
}
export function setCalculatorMonthlyIngredientSpending(monthlyIngredient) {
  return {
    type: types.SET_CALCULATOR_MONTHLY_INGREDIENT_SPENDING,
    monthlyIngredient,
  };
}
export function setCalculatorFullEmployees(employees) {
  return {
    type: types.SET_CALCULATOR_FULLTIME_EMPLOYEES,
    employees,
  };
}

export function fetchCalculatorPageInfo() {
  return (dispatch) => {
      // TODO SE PODRIA CHACHEAR EN EL CLIENTE POR UN DETERMINADO TIEMPO
    axios
      .get(URIS.CALCULATOR_PAGE)
      .then((response) => {
        console.debug(response.data);
        dispatch(setCalculatorPageTitle(response.data.calculator.title));
        dispatch(
          setCalculatorPageDescription(response.data.calculator.description)
        );
      })
      .catch((error) => {
        console.error(error);
        // TODO Agregar manejo de errores
      });
  };
}
