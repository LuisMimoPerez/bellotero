import "../styles/main-styles.scss";
import { Provider } from "react-redux";
import { useStore } from "../store/store";
import Layout from "../components/Layout/Layout";

function MyApp({ Component, pageProps }) {
  const store = useStore(pageProps.initialReduxState);
  return (
    <Provider store={store}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </Provider>
  );
}

export default MyApp;
