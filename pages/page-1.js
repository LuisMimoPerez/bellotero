import Head from "next/head";
import TestimonialsContainer from "../containers/Testimonials/Testimonials";
function testimonials(props) {
  return (
    <>
      <Head>
        <title>Testimonials</title>
        <meta name="description" content="Bellotero io" />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/favicon-16x16.png"
        />
        <link rel="manifest" href="/site.webmanifest" />
      </Head>

      <TestimonialsContainer />
    </>
  );
}

testimonials.propTypes = {};

export default testimonials;
