import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";

import Header from "../Header/Header";
import { fetchHeaderOptions } from "../../store/header/headerActions";

function Layout({ children }) {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchHeaderOptions());
      }, []);
  return (
    <div>
      <Header />
      {children}
    </div>
  );
}

Layout.propTypes = {
  children: PropTypes.element,
};

export default Layout;
