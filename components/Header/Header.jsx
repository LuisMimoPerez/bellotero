import React from "react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import clsx from "clsx";

import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import { headerNavOptions } from "../../store/header/headerSelectors";

function Header(props) {
  const router = useRouter();
  const rdxHeaderRoutes = useSelector(headerNavOptions);

  return (
    <header id="page-header">
      <div className="logo-container">
        <Link href="/">
          <a>
            <Image src="/bellotero.svg" alt="logo" width="133" height="26" layout="fixed" />
          </a>
        </Link>
      </div>
      <div className="nav-container">
        <ul className="nav-wrapper">
          {rdxHeaderRoutes.map((r, index) => (
            <li
              key={`route_${index}`}
              className={clsx(router.pathname.startsWith("/"+r.route) && "active")}
            >
              <Link href={r.route}>
                <a className="Text-Style-2">{r.text}</a>
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </header>
  );
}

Header.propTypes = {};

export default Header;
