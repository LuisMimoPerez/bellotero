import React from "react";
import PropTypes from "prop-types";
import Slider from "react-rangeslider";
import "react-rangeslider/lib/index.css";

function CustomRange({ step, min, max, onChange, value }) {
  return (
    <div id="slider-range">
      <Slider
        value={value}
        orientation="horizontal"
        onChange={onChange}
        step={step}
        min={min}
        max={max}
        tooltip={false}
      />
    </div>
  );
}

CustomRange.propTypes = {};

export default CustomRange;
