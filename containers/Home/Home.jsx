import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import CustomRange from "../../components/CustomRange/CustomRange";
import { useDispatch, useSelector } from "react-redux";
import {
  calculatorPageDescription,
  calculatorPageFullTimeEmployees,
  calculatorPageMonthlyIngredientSpending,
  calculatorPageTitle,
} from "../../store/pages/pagesSelectors";
import {
  fetchCalculatorPageInfo,
  setCalculatorFullEmployees,
  setCalculatorMonthlyIngredientSpending,
} from "../../store/pages/calculatorActions";

const FOOD_SAVING_FACTOR = 0.3;
const ANNUAL_SAVING_FACTOR = 1337;
function Home(props) {
  const dispatch = useDispatch();
  const rdxCalculatorPageTitle = useSelector(calculatorPageTitle);
  const rdxCalculatorPageDescription = useSelector(calculatorPageDescription);
  const rdxCalculatorPageMonthlyIngredientSpending = useSelector(
    calculatorPageMonthlyIngredientSpending
  );
  const rdxCalculatorPageFullTimeEmployees = useSelector(
    calculatorPageFullTimeEmployees
  );

  const [esFoodCostSaving, setEsFoodCostSaving] = useState(
    getEstimatedFoodCostSaving()
  );
  const [esAnnualSavings, setEsAnnualSavings] = useState(
    getEstimatedAnnualSavings()
  );

  useEffect(() => {
    dispatch(fetchCalculatorPageInfo());
  }, []);

  useEffect(() => {
    if (rdxCalculatorPageMonthlyIngredientSpending) {
      setEsFoodCostSaving(getEstimatedFoodCostSaving());
    }
  }, [rdxCalculatorPageMonthlyIngredientSpending]);

  useEffect(() => {
    if (esFoodCostSaving && rdxCalculatorPageFullTimeEmployees) {
      setEsAnnualSavings(getEstimatedAnnualSavings());
    }
  }, [esFoodCostSaving, rdxCalculatorPageFullTimeEmployees]);

  function getEstimatedFoodCostSaving() {
    return rdxCalculatorPageMonthlyIngredientSpending * FOOD_SAVING_FACTOR;
  }

  function getEstimatedAnnualSavings() {
    return (
      rdxCalculatorPageFullTimeEmployees * ANNUAL_SAVING_FACTOR +
      esFoodCostSaving
    );
  }

  function setMonthlyIngredientSpent(spent) {
    dispatch(setCalculatorMonthlyIngredientSpending(spent));
  }

  function setEmployeesNumber(number) {
    dispatch(setCalculatorFullEmployees(number));
  }

  return (
    <section id="home-container" className="bellotero-background">
      <div className="xl-spacing" />
      <div className="home-wrapper">
        <div className="description-container">
          <div className="description-wrapper">
            <div className="title-container">
              <h1>
                {rdxCalculatorPageTitle.split(" ").slice(0, 3).join(" ") || ""}
              </h1>
              <div className="xs-spacing" />
              <h1>
                {rdxCalculatorPageTitle.split(" ").slice(3, 4).join(" ") || ""}
              </h1>
            </div>
            <div className="md-spacing" />
            <div>
              <p className="Text-Style-3">{rdxCalculatorPageDescription}</p>
            </div>
            <div className="md-spacing" />
          </div>
        </div>
        <div className="calculator-container">
          <div className="range-component">
            <div className="description-container">
              <p className="Text-label">Monthly ingredient spending</p>
              <div>
                <div className="input-group">
                  <span className="input-text">$</span>
                  <input
                    type="text"
                    readOnly
                    value={rdxCalculatorPageMonthlyIngredientSpending}
                  />
                </div>
              </div>
            </div>
            <div className="md-spacing" />
            <div className="range-container">
              <CustomRange
                step={1}
                min={10}
                max={100}
                value={rdxCalculatorPageMonthlyIngredientSpending}
                onChange={setMonthlyIngredientSpent}
              />
            </div>
          </div>
          <div className="lg-spacing" />
          <div className="range-component">
            <div className="description-container">
              <p className="Text-label">
                Full-time employees that process invoices
              </p>
              <div>
                <div className="input-group">
                  <input
                    type="text"
                    readOnly
                    value={rdxCalculatorPageFullTimeEmployees}
                    style={{ maxWidth: "100px" }}
                  />
                </div>
              </div>
            </div>
            <div className="md-spacing" />
            <div className="range-container">
              <CustomRange
                step={1}
                min={1}
                max={10}
                value={rdxCalculatorPageFullTimeEmployees}
                onChange={setEmployeesNumber}
              />
            </div>
          </div>
          <div className="lg-spacing" />
          <div className="totals-container">
            <div className="total-text">
              <p>
                <span>$</span>
                {esFoodCostSaving.toFixed(2)}
              </p>
              <p className="Text-label">Estimated cost food savings</p>
            </div>
            <div>
              <p className="total-text">
                <span>$</span>
                {esAnnualSavings.toFixed(2)}
              </p>
              <p className="Text-label">Your estimated annual savings</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

Home.propTypes = {};

export default Home;
