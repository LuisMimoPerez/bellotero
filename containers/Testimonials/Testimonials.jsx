import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import {
  testimonialsPageTestimonials,
  testimonialsPageTitle,
} from "../../store/pages/pagesSelectors";
import { fetchTestimonialPageInfo } from "../../store/pages/testimonialsActions";
import clsx from "clsx";

function Testimonials() {
  const dispatch = useDispatch();
  const rdxTestimonialPageTitle = useSelector(testimonialsPageTitle);
  const rdxTestimonialPageTestimonials = useSelector(
    testimonialsPageTestimonials
  );
  const [activeSlideIndex, setActiveSlideIndex] = useState(0);

  useEffect(() => {
    dispatch(fetchTestimonialPageInfo());
  }, []);

  function handleSliderArrowClick(arrowOrientation) {
    if (arrowOrientation === "right") {
      if (activeSlideIndex + 1 < rdxTestimonialPageTestimonials.length) {
        setActiveSlideIndex(activeSlideIndex + 1);
      }
    } else if (arrowOrientation === "left") {
      if (activeSlideIndex - 1 >= 0) {
        setActiveSlideIndex(activeSlideIndex - 1);
      }
    }
  }

  return (
    <section id="testimonials-container" className="bellotero-background">
      <div className="xl-spacing" />
      <div>
        <div className="customer-love-us-container">
          <h1>{rdxTestimonialPageTitle}</h1>
        </div>
      </div>
      <div className="lg-spacing" />
      <div className="testimonials-slider-container">
        {rdxTestimonialPageTestimonials.map((tes, index) => (
          <div
            key={`testimonial_${index}`}
            className={clsx(
              "slide-container",
              index === activeSlideIndex && "active"
            )}
          >
            <div className="author-info-container">
              <p>{tes.name}</p>
              <small>{tes.position}</small>
            </div>
            <div className="md-spacing" />
            <div className="author-testimonial-container">
              <p>{tes.comment}</p>
            </div>
          </div>
        ))}
        <div className="slider-controls">
          <div className="paginator">
            <p>
              {activeSlideIndex + 1}/{rdxTestimonialPageTestimonials?.length}
            </p>
          </div>
          <div className="arrows">
            <div onClick={() => handleSliderArrowClick("left")}>
              <p>&larr;</p>
            </div>
            <div onClick={() => handleSliderArrowClick("right")}>
              <p>&rarr;</p>
            </div>
          </div>
        </div>
      </div>
      <div className="md-spacing" />

    </section>
  );
}

Testimonials.propTypes = {};

export default Testimonials;
