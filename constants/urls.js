export const URIS ={
    HEADER_ROUTER: "https://raw.githubusercontent.com/Bernabe-Felix/Bellotero/master/app.json",
    CALCULATOR_PAGE: "https://raw.githubusercontent.com/Bernabe-Felix/Bellotero/master/page2.json",
    TESTIMONIALS_PAGE: "https://raw.githubusercontent.com/Bernabe-Felix/Bellotero/master/page1.json"
}